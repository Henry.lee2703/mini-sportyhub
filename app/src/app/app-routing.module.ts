// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';
// import { PitchComponent } from './pitch/pitch.component';
// import { PitchAddComponent } from './pitch-add/pitch-add.component'
// import { PitchDetailComponent } from './pitch-detail/pitch-detail.component';
// import { PitchEditComponent } from './pitch-edit/pitch-edit.component';
//
// import { HeaderComponent } from './header/header.component';

// const routerConfigs: Routes = [
//   { path: 'new', component: PitchAddComponent },
//   { path: 'center', component: PitchComponent },
//   { path: 'detail/:id', component: PitchDetailComponent },
//   { path: '', redirectTo: '/center', pathMatch: 'full' },
//   { path: 'edit/:id', component: PitchEditComponent }
// ];


// @NgModule({
//   declarations: [
//     PitchAddComponent,
//      PitchComponent,
//      PitchDetailComponent,
//      PitchEditComponent,
//     HeaderComponent
//   ],
//   imports: [
//     RouterModule.forRoot(routerConfigs),     
//
//   ],
//   exports:[RouterModule]
// })
// export class AppRoutingModule { }
// export const routingComponents = [ PitchAddComponent, PitchComponent,PitchEditComponent,PitchDetailComponent,HeaderComponent]
