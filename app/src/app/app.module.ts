import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'
import { Routes, RouterModule } from '@angular/router'
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';

import { environment } from '../environments/environment';
import { PitchAddComponent } from './pitch-add/pitch-add.component'
import { PitchDetailComponent } from './pitch-detail/pitch-detail.component';
import { PitchComponent } from './pitch/pitch.component';
import { PitchEditComponent } from './pitch-edit/pitch-edit.component';
import { MaterialModule } from './material/material.module';
import { HeaderComponent } from './header/header.component';
import { NotificationComponent } from './notification/notification.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

// import { AppRoutingModule } from './app-routing.module';
// import { routingComponents } from './app-routing.module'

const routerConfigs: Routes = [
  { path: 'new', component: PitchAddComponent },
  { path: 'center', component: PitchComponent },
  { path: 'detail/:id', component: PitchDetailComponent },
  { path: '', redirectTo: '/center', pathMatch: 'full' },
  { path: 'edit/:id', component: PitchEditComponent },
  { path:'**', component: PageNotFoundComponent },
];


@NgModule({
  declarations: [
    AppComponent,
    // routingComponents,
   PitchAddComponent,
   PitchComponent,
   PitchDetailComponent,
   PitchEditComponent,
   HeaderComponent,
   NotificationComponent,
   PageNotFoundComponent
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
     MaterialModule,
   // AppRoutingModule,
   RouterModule.forRoot(routerConfigs),
    HttpClientModule,
    ReactiveFormsModule,
    CommonModule,

  ],
  entryComponents:[NotificationComponent],// Need to display Dialog
  providers: [{ provide: NG_ENTITY_SERVICE_CONFIG, useValue: { baseUrl: 'https://jsonplaceholder.typicode.com' } }],
  bootstrap: [AppComponent]
})
export class AppModule { }
