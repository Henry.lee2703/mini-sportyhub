import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PitchAddComponent } from './pitch-add.component';

describe('PitchAddComponent', () => {
  let component: PitchAddComponent;
  let fixture: ComponentFixture<PitchAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PitchAddComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PitchAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
