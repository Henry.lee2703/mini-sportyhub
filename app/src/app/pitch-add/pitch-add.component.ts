import { Component, OnInit } from '@angular/core';

import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { PitchsService } from '../state/pitchs.service'
import { Pitch } from '../state/pitch.model'
import { PitchsQuery } from '../state/pitchs.query'
import { PitchsStore } from '../state/pitchs.store';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-pitch-add',
  templateUrl: './pitch-add.component.html',
  styleUrls: ['./pitch-add.component.scss']
})

export class PitchAddComponent implements OnInit {
  id1: number;

  constructor(private pitchQuery: PitchsQuery,
    private pitchService: PitchsService,
    private pitchsStore: PitchsStore,
    private fb: FormBuilder) {
    // this.postForm=  this.form.group({ type: this.form.array([])})
    // this.pitchService.get()
  }
  id: number
  public postForm = new FormGroup({
    name: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    banner: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required),
    // type: new FormArray([], [Validators.required]), 
    type1: new FormControl('', Validators.required),
    type2: new FormControl(''),
    type3: new FormControl('')
  })



  onAdd(formData: Pitch) {
    let value1 = ""
    let value2 = ""
    let value3 = ""
    if (formData.type1.toString() == "true") {
      value1 = " Sân 5"
    }
    else value1 = ""

    console.log(value1);
    if (formData.type2.toString() == "true") {
      value2 = "Sân 7"
    }
    else
      value2 = ""

    if (formData.type3.toString() == "true") {
      value3 = "Sân 10"
    }
    else
      value3 = ""

    this.pitchQuery.selectAll().subscribe(count => this.id1 = count.length + 1) // Query size of Store

    let newPitch = {
      id: this.id1,
      banner: formData.banner,
      address: formData.address,
      name: formData.name,
      phoneNumber: formData.phoneNumber,
      type1: value1,
      type2: value2,
      type3: value3,
    }
    console.log(newPitch)
    this.pitchService.add(newPitch)
    console.log(this.pitchsStore["storeValue"]["entities"])
    console.log('123vuvuvu')
  }
  //  checkCheckBoxvalue(event){

  //    if(event.checked==='true')
  //  console.log('vu')
  // }


  ngOnInit(): void {
    console.log('query here')
  }

}


  // Data: Array<any> = [
  //   { des: 'sân 5', value: 'sân 5' },
  //   { des: 'sân 7', value: 'sân 7' },
  //   { des: 'sân 10', value: 'sân 10' },
  // ];
//   options = ['Sân 5', 'Sân 7', 'Sân 10'];
//   optionsMap = {
//           type1: false,
//           type2: false,
//           type3: false,
//   };
//   optionsChecked = [];


//   initOptionsMap() {
//     for (var x = 0; x<this.order.options.length; x++) {
//         this.optionsMap[this.options[x]] = true;
//     }
// }

// updateCheckedOptions(option, event) {
//   this.optionsMap[option] = event.target.checked;
// }

// updateOptions() {
//   for(var x in this.optionsMap) {
//       if(this.optionsMap[x]) {
//           this.optionsChecked.push(x);
//       }  
//   }
//   this.options = this.optionsChecked;
//   this.optionsChecked = [];
// }

  // get Types(): FormArray {
  //   return this.postForm.get('type') as FormArray;
  // }

  // addTypes() {
  //   this.Types.push(this.fb.control(''));
  // }

  // removeType(index: number) {
  //   this.Types.removeAt(index);
  // }


