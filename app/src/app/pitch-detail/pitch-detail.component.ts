import { Component, OnInit } from '@angular/core';

import { PitchsService } from '../state/pitchs.service'
import { Pitch } from '../state/pitch.model'
import { PitchsQuery } from '../state/pitchs.query'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-pitch-detail',
  templateUrl: './pitch-detail.component.html',
  styleUrls: ['./pitch-detail.component.scss']
})
export class PitchDetailComponent implements OnInit {


  //  readonly _id =  this.route.params.subscribe( params => console.log(params));

  constructor(
    private pitchService: PitchsService,
    private pitchQuery: PitchsQuery,
    private route: ActivatedRoute) { }

  pitchDetail: Pitch
  private detail: number

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.pitchDetail = this.pitchQuery.getEntity(params.id)
      console.log(params.id)
    });
  }

}

