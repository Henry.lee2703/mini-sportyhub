import { Component, OnInit } from '@angular/core';
import { PitchsService } from '../state/pitchs.service';
import { PitchsQuery } from '../state/pitchs.query';
import { ActivatedRoute } from '@angular/router';
import { Pitch } from '../state/pitch.model';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { PitchsStore } from '../state/pitchs.store';
import { NotificationComponent } from '../notification/notification.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-pitch-edit',
  templateUrl: './pitch-edit.component.html',
  styleUrls: ['./pitch-edit.component.scss']
})
export class PitchEditComponent implements OnInit {
  public _id: number
  pitchEdit: Pitch
  editForm: FormGroup;

  constructor(
    private pitchService: PitchsService,
    private pitchQuery: PitchsQuery,
    private route: ActivatedRoute,
    private pitchsStore: PitchsStore,
    public dialog: MatDialog
  ) {
    this.route.params.subscribe(params => {
      this.pitchEdit = this.pitchQuery.getEntity(params.id)
      this._id = +this.pitchEdit.id
      console.log(this.pitchEdit.id);
      console.log(this._id)
    }
    );
    let value1 = ""
    let value2 = ""
    let value3 = ""

    this.editForm = new FormGroup({
      name: new FormControl(this.pitchEdit.name, Validators.required),
      address: new FormControl(this.pitchEdit.address, Validators.required),
      banner: new FormControl(this.pitchEdit.banner, Validators.required),
      phoneNumber: new FormControl(this.pitchEdit.phoneNumber, Validators.required),
      // type: this.fb.array([], [Validators.required]), 
      type1: new FormControl(this.pitchEdit.type1.toString()),
      type2: new FormControl(this.pitchEdit.type2.toString()),
      type3: new FormControl(this.pitchEdit.type3.toString())
    });

  }

  Edit() {
    //   if(this.editForm.type1.toString()=="Sân 5"){

    //   }
    //  else value1=""

    //  console.log(value1);
    //  if(this.pitchEdit.type2.toString()=="true"){
    //    "Sân 7"
    //  }
    //  else 
    //  value2=""

    //  if(this.pitchEdit.type3.toString()=="true"){
    //    value3="Sân 10"
    //  }
    //  else 
    //  value3=""

    console.log('this.editForm.value: ', this.editForm.value);
    this.pitchService.update(this._id, this.editForm.value)
    // this.pitchQuery.selectAll().subscribe((data) => console.log(`data: `, data))
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NotificationComponent, {
      width: '400px',
      data: "Do you confirm the deletion of this pitch?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.pitchService.remove(this._id)
        console.log(this.pitchsStore["storeValue"]["entities"])
        console.log('Yes clicked');
        // DO SOMETHING
      }
    });

  }


  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.pitchEdit = this.pitchQuery.getEntity(params.id)
      this._id = +this.pitchEdit.id
      console.log(this.pitchEdit.id);
      console.log(this._id)
      console.log('type', this.pitchEdit.type1)
    }
    );
  }
}


/*
  Pitch => EditForm

  dateChanges => update form EditForm

  EditForm => Pitch
*/




// openDialog(): void {
//   this.pitchService.remove(this._id)
//   console.log(this.pitchsStore["storeValue"]["entities"])
// }
























