import { Component, OnInit } from '@angular/core';


import { PitchsService } from '../state/pitchs.service'
import { Pitch } from '../state/pitch.model'
import { PitchsQuery } from '../state/pitchs.query'
import { identifierModuleUrl } from '@angular/compiler';
import { PitchsStore } from '../state/pitchs.store';
// import { resolve } from 'dns';

@Component({
  selector: 'app-pitch',
  templateUrl: './pitch.component.html',
  styleUrls: ['./pitch.component.scss']
})
export class PitchComponent implements OnInit {
  // pitch: FormGroup
  listPitch: Pitch[]
  listLocal: Pitch[];

  constructor(private pitchService: PitchsService,
    private pitchQuery: PitchsQuery,
    private pitchsStore: PitchsStore) {

  }

  ngOnInit(): void {
    this.pitchService.get()
    this.pitchQuery.selectAll({ sortBy: "id" }).subscribe(data => {
      console.log('data', data)
      this.listPitch = data
      console.log('truy van 1')
      localStorage.setItem('listPitchs', JSON.stringify(this.listPitch.reverse()))
      this.listLocal = JSON.parse(localStorage.getItem('listPitchs'))
      console.log(this.listLocal)
    })
  }
}
