import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { PitchsStore, PitchsState } from './pitchs.store';

@Injectable({ providedIn: 'root' })
export class PitchsQuery extends QueryEntity<PitchsState> {

  constructor(protected store: PitchsStore) {
    super(store);
  }

}
