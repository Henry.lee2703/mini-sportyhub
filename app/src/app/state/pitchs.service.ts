import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { HttpClient } from '@angular/common/http';
import { PitchsStore } from './pitchs.store';
import { Pitch } from './pitch.model';
import { tap } from 'rxjs/operators';
import { PitchsQuery } from './pitchs.query';



@Injectable({ providedIn: 'root' })
export class PitchsService {

  localArr: []
  list: [];
  constructor(private pitchsStore: PitchsStore,
    private http: HttpClient,
    private pitchsQuery: PitchsQuery) {
  }
  // private _url = 'http://localhost:3000/pitchs'

  get() {
    let list=localStorage.getItem('listPitchs')
    this.pitchsStore.set(JSON.parse(list))
 
  }

  add(pitch: Pitch) {
    this.pitchsStore.add(pitch);
  
  }

  update(id: any, pitch: Partial<Pitch>) {
    this.pitchsStore.update(id, pitch);
  }

  remove(id: ID) {
    this.pitchsStore.remove(id);
  }
}
