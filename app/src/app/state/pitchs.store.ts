import { Injectable } from '@angular/core';
import { Pitch } from './pitch.model';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';

export interface PitchsState extends EntityState<Pitch> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'pitchs' })
export class PitchsStore extends EntityStore<PitchsState> {

  constructor() {
    super();
  }

    

}

